class ExhaustiveSearch:
	def solve(self, A, M):
		for m in M:
			if self.search(0, m):
				print("เจอ")
			else:
				print("ไม่เจอ")

	def search(self, i, m):
		if m == 0:
			return True
		elif i >= n:
			return False
	
		return self.search(i + 1, m) or self.search(i + 1, m - A[i])

if __name__ == '__main__':
	n = int(input('กำหนดว่าคานี้จะรับกี่ตัว'))
	A = [int(i) for i in input('ใส่ค่า เช่น 2 3 4 5:').rstrip().split(" ")]
	q = int(input('กำหนดว่าคานี้จะรับกี่ตัว (2)'))
	M = [int(i) for i in input('ใส่ค่า เช่น 2 3 4 5:').rstrip().split(" ")]

	x = ExhaustiveSearch()
	x.solve(A, M)

print(A)
print(M)